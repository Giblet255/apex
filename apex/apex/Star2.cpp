#include "Star2.h"
#include <cstdlib>

Star2::Star2(sf::Texture& star2Texture, sf::Vector2u newScreenBounds)
{
	sprite.setTexture(star2Texture);
	screenBounds = newScreenBounds;
	speed = 250;
	// Avoid duplicating code by calling reset here instead of positioning manually
	Reset();
}

void Star2::Reset()
{
	// Choose a random starting position
	sprite.setPosition(sf::Vector2f(rand() % screenBounds.x, rand() % screenBounds.y));
	// Choose a random scale (between 1 and 1/4th scale)
	float scaleFactor = 0.5f / (0.5f + rand() % 4);
	sprite.setScale(scaleFactor, scaleFactor);
}

void Star2::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + sf::Vector2f(-speed, 0) * frameTime.asSeconds();
	// Have we gone off the screen completely?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0)
	{
		// Then wrap around back to the right
		newPosition.x = screenBounds.x;
	}
	// Move to the new position
	sprite.setPosition(newPosition);
}



void Star2::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}
