
#include "Player.h"
#include "Bullet.h"


Player::Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer&firingSoundBuffer)
	: sprite(playerTexture)
	, velocity(0.0f, 0.0f)
	, speed(600.0f)
	, screenSize(newScreenBounds)
	, bullets(newBullets)
	, bulletTexture(newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(0.05f))
	, bulletFireSound(firingSoundBuffer)
{
	// Avoid duplicating code by calling reset here instead of positioning the player manually
	//Reset();

	
	sprite.setPosition(
	10 , 
		screenSize.y / 2 - playerTexture.getSize().y / 2
	);

}



void Player::Input()
{
	// Player keybind input
	// Start by zeroing out player velocity
	velocity.x = 0.0f;
	velocity.y = 0.0f;

	// If the player is pressing space, spawn a bullet
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space) && bulletCooldownRemaining <= sf::seconds(0.0f))
	{

		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture.getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture.getSize().x / 2;
		bullets.push_back(Bullet(bulletTexture, screenSize, bulletPosition, sf::Vector2f(1000, 0)));
		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
		// Play firing sound
		bulletFireSound.play();
	}



	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// Move player right
		velocity.x = speed;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	{
		// Move player up
		velocity.y = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	{
		// Move player left
		velocity.x = -speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down))
	{
		// Move player down
		velocity.y = speed;
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	{
		// Move player right
		velocity.x = speed;
	}

	


}
void Player::Update(sf::Time frameTime)
{
	sf::Vector2f NewPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	
	if (NewPosition.x < 0)
		NewPosition.x = 0;
	if (NewPosition.x + sprite.getTexture()->getSize().x > screenSize.x)
		NewPosition.x = screenSize.x - sprite.getTexture()->getSize().x;
	
	if (NewPosition.y < 0)
		NewPosition.y = 0;
	
	//if (NewPosition.y + sprite.getTexture()->getSize().y > screenSize.y-35)
	//	NewPosition.y = screenSize.y - sprite.getTexture()->getSize().y-35;

	if (NewPosition.y + sprite.getTexture()->getSize().y > screenSize.y )
		NewPosition.y = screenSize.y - sprite.getTexture()->getSize().y;



	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	// Move the player
	sprite.setPosition(NewPosition);
}



void Player::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

sf::FloatRect Player::GetHitBox()
{
	return sprite.getGlobalBounds();
}