#pragma once
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp> // this load in the sound buffers
// Library for handling collections of objects
#include <vector>
#include "Bullet.h"
class Player
{
public: // access level (to be discussed later)
// Constructor
	Player(sf::Texture& playerTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer&firingSoundBuffer);
	// Functions to call Player-specific code
	void Input();
	void Update(sf::Time frameTime);
	// Variables (data members) used by this class
	void Draw(sf::RenderWindow& gameWindow);

	// Getter

	sf::FloatRect GetHitBox();
	
private:
	
	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>& bullets;
	sf::Texture& bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;

};
