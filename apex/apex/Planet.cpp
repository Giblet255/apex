#include "Planet.h"
#include <cstdlib>

Planet::Planet(sf::Texture& PlanetTexture, sf::Vector2u newScreenBounds)
{
	sprite.setTexture(PlanetTexture);
	screenBounds = newScreenBounds;
	speed = 200;
	// Avoid duplicating code by calling reset here instead of positioning manually
	Reset();
}

void Planet::Reset()
{
	// Choose a random Planetting position
	sprite.setPosition(sf::Vector2f(rand() % screenBounds.x, rand() % screenBounds.y));
	// Choose a random scale (between 1 and 1/4th scale)
	float scaleFactor = 0.25f / (0.25f + rand() % 2);
	sprite.setScale(scaleFactor, scaleFactor);
}

void Planet::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + sf::Vector2f(-speed, 0) * frameTime.asSeconds();
	// Have we gone off the screen completely?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0)
	{
		// Then wrap around back to the right
		newPosition.x = screenBounds.x;
	}
	// Move to the new position
	sprite.setPosition(newPosition);
}



void Planet::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
}