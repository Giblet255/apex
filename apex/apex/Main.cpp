#include <SFML/Graphics.hpp> // this is the SFML package 
#include <SFML/Audio.hpp> // this load in the sound buffers
#include <string>
#include <vector>
#include "Player.h"
#include "Star.h"
#include "Star2.h"
#include "Planet.h"
#include "Bullet.h"
#include "Ememy.h"
#include "SpawnData.h"
#include <cstdlib> // we are using this to gen random Numbers
#include <time.h> // see above

// The main() Function - entry point for our program
int main()
{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;
	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode::getDesktopMode(), "Apex By Scott Gilbert", sf::Style::Titlebar | sf::Style::Close);
	//gameWindow.create(sf::VideoMode::getDesktopMode(), "Apex By Scott Gilbert", sf::Style::Fullscreen);
	
	// -----------------------------------------------
	// Game Setup
	// -----------------------------------------------
	

	// Bullets
	std::vector<Bullet> bullets;
	sf::Texture playerBulletTexture;
	playerBulletTexture.loadFromFile("Assets/Graphics/playerBullet2.png");

	// Bullets
	std::vector<Bullet> enemybullets;
	sf::Texture enemyBulletTexture;
	enemyBulletTexture.loadFromFile("Assets/Graphics/enemyBullet2.png");



	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer firingSoundBuffer;
	firingSoundBuffer.loadFromFile("Assets/Audio/fire.ogg");

	// Player
	sf::Texture playerTexture;
	playerTexture.loadFromFile("Assets/Graphics/player2b.png");
	Player playerObject(playerTexture, gameWindow.getSize(), bullets, playerBulletTexture,firingSoundBuffer);

	// Movment patterns 

	std::vector<sf::Vector2f> ZizagPattern ;
	ZizagPattern.push_back(sf::Vector2f(1500, 0));
	ZizagPattern.push_back(sf::Vector2f(900, 900));
	ZizagPattern.push_back(sf::Vector2f(300, 0));

	// Movment patterns 

	std::vector<sf::Vector2f> Rush;
	Rush.push_back(sf::Vector2f(1500, 500));
	Rush.push_back(sf::Vector2f(900, 100));
	Rush.push_back(sf::Vector2f(300, 1500));

	// Movment patterns 

	std::vector<sf::Vector2f> Rush2;
	Rush2.push_back(sf::Vector2f(750, 300));
	Rush2.push_back(sf::Vector2f(1500, 600));
	Rush2.push_back(sf::Vector2f(1500, 1200));
	//----
	std::vector<sf::Vector2f> line1;
	line1.push_back(sf::Vector2f(2500, 500));
	line1.push_back(sf::Vector2f(-500, 500));
	
	std::vector<sf::Vector2f> line2;
	line2.push_back(sf::Vector2f(2500, 900));
	line2.push_back(sf::Vector2f(-500, 900));

	std::vector<sf::Vector2f> line3;
	line3.push_back(sf::Vector2f(2500, -500));
	line3.push_back(sf::Vector2f(200, 1500));

	std::vector<sf::Vector2f> line4;
	line4.push_back(sf::Vector2f(2500, 1500));
	line4.push_back(sf::Vector2f(200, -500));


	// Ememy

	std::vector<Ememy>enemies;
	sf::Texture EmemyTexture;
	EmemyTexture.loadFromFile("Assets/Graphics/badguy1b.png");
	
	//enemies.push_back(Ememy(EmemyTexture, gameWindow.getSize(), enemybullets, enemyBulletTexture, firingSoundBuffer, ZizagPattern));


	// Spawn Info 

	std::vector<Spawndata> spawndata;
	int spawnIndex = 0; // this tell us where we are on list 

	spawndata.push_back({ sf::Vector2f(1500,1000),
		ZizagPattern,
		sf::seconds(3.0f) });

	spawndata.push_back({ sf::Vector2f(0,1000),
		Rush,
		sf::seconds(0.0f) });

	spawndata.push_back({ sf::Vector2f(1500,1000),
		ZizagPattern,
		sf::seconds(7.0f) });

	spawndata.push_back({ sf::Vector2f(100,1000),
		Rush,
		sf::seconds(0.0f) });

	spawndata.push_back({ sf::Vector2f(1500,1000),
		ZizagPattern,
		sf::seconds(7.0f) });

	spawndata.push_back({ sf::Vector2f(200,1000),
		Rush,
		sf::seconds(0.0f) });

	spawndata.push_back({ sf::Vector2f(1500,1000),
		Rush,
		sf::seconds(7.0f) });

	spawndata.push_back({ sf::Vector2f(0,1000),
		Rush2,
		sf::seconds(0.0f) });

	spawndata.push_back({ sf::Vector2f(1500,1000),
		ZizagPattern,
		sf::seconds(7.0f) });

	spawndata.push_back({ sf::Vector2f(100,1000),
		Rush2,
		sf::seconds(0.0f) });

	spawndata.push_back({ sf::Vector2f(1500,1000),
		Rush,
		sf::seconds(7.0f) });

	spawndata.push_back({ sf::Vector2f(200,1000),
		Rush2,
		sf::seconds(0.0f) });
	//----
	spawndata.push_back({ sf::Vector2f(2000,500),
		line1,
		sf::seconds(15.0f) });
	spawndata.push_back({ sf::Vector2f(2000,500),
		line1,
		sf::seconds(0.5f) });
	spawndata.push_back({ sf::Vector2f(2000,500),
		line1,
		sf::seconds(0.5f) });
	//---
	spawndata.push_back({ sf::Vector2f(2000,500),
		line2,
		sf::seconds(12.5f) });
	spawndata.push_back({ sf::Vector2f(2000,500),
		line2,
		sf::seconds(0.5f) });
	spawndata.push_back({ sf::Vector2f(2000,500),
		line2,
		sf::seconds(0.5f) });
	spawndata.push_back({ sf::Vector2f(2000,500),
		line2,
		sf::seconds(0.5f) });
	// ------ 
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(12.5f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	//--
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(2.5f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });
	spawndata.push_back({ sf::Vector2f(2000,0),
		line3,
		sf::seconds(1.0f) });
	spawndata.push_back({ sf::Vector2f(2000,1000),
		line4,
		sf::seconds(0.0f) });


	sf::Time timeTospawn = spawndata[0].delay;

	// Gameover Bool 

	bool gameOver = false;
	bool Win = false;





	// Stars
	sf::Texture starTexture;
	starTexture.loadFromFile("Assets/Graphics/star2.png");
	std::vector<Star> stars;
	int numStars = 35;
	for (int i = 0; i < numStars; ++i)
	{
		stars.push_back(Star(starTexture, gameWindow.getSize()));
	}

	// Planets
	sf::Texture PlanetTexture;
	PlanetTexture.loadFromFile("Assets/Graphics/star3.png");
	std::vector<Planet> Planets;
	int numPlanets = 35;
	for (int i = 0; i < numPlanets; ++i)
	{
		Planets.push_back(Planet(PlanetTexture, gameWindow.getSize()));
	}

	// Stars2
	sf::Texture star2Texture;
	star2Texture.loadFromFile("Assets/Graphics/star.png");
	std::vector<Star2> stars2;
	int numStars2 = 35;
	for (int i = 0; i < numStars2; ++i)
	{
		stars2.push_back(Star2(star2Texture, gameWindow.getSize()));
	}



	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	// Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// Start our music playing
	gameMusic.setVolume(60.0f);
	gameMusic.play();


	// Setting Game Font and Txt ----------------------
	int Playerscore = 0;
	sf::Font gameFont;
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: " + std::to_string(Playerscore));
	scoreText.setCharacterSize(20);
	scoreText.setPosition(100, 100);


	// Setting Game over Messgage

	sf::Text gameOverText;
	gameOverText.setFont(gameFont);
	gameOverText.setString("Game over please press R to Reset ");
	gameOverText.setCharacterSize(48);
	gameOverText.setPosition(500, 500);
	gameOverText.setFillColor(sf::Color::Red);


	sf::Text WinText;
	WinText.setFont(gameFont);
	WinText.setString(" Congraz You Are a god i bow before plase dont hurt me ");
	WinText.setCharacterSize(48);
	WinText.setPosition(500, 500);
	WinText.setFillColor(sf::Color::Cyan);



	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	// Create a time value to store the total time limit for our game
	sf::Time timeLimit = sf::seconds(60.0f);
	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;
	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;
	
	
	//++++++++++++++++++++++++++++++++++++++++++++++++ Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// -----------------------------------------------
		// Input Section
		// -----------------------------------------------
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed
			// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}
		// End event polling loop
		// -----------------------------------------------
		// Player keybind input
		playerObject.Input();
		// Update Section
		// -----------------------------------------------
		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;
		
		// If in game over Stat 

		if (gameOver || Win)
		{
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
			{
				gameOver = false;
				Win = false;
				Playerscore = 0; 
				gameMusic.play();
				enemies.clear();
				enemybullets.clear();
				bullets.clear();
				spawnIndex = 0;

			}
		}


		// TODO: Update game state
		// -----------------------------------------------

		if (gameOver == false && !Win) {


			// Move the player
			playerObject.Update(frameTime);
			scoreText.setString("Score: " + std::to_string(Playerscore));


			// Update the Planets
			for (int i = 0; i < Planets.size(); ++i)
			{
				Planets[i].Update(frameTime);
			}

			// Update the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].Update(frameTime);
			}

			// Update the stars
			for (int i = 0; i < stars2.size(); ++i)
			{
				stars2[i].Update(frameTime);
			}


			// Update the bullets
			for (int i = bullets.size() - 1; i >= 0; --i)
			{
				bullets[i].Update(frameTime);
				// If the bullet is dead, delete it
				if (!bullets[i].GetAlive())
				{
					// Remove the item from the vector
					bullets.erase(bullets.begin() + i);
				}
			}


			// Update badguyz the bullets
			for (int i = enemybullets.size() - 1; i >= 0; --i)
			{
				enemybullets[i].Update(frameTime);
				// If the bullet is dead, delete it
				if (!enemybullets[i].GetAlive())
				{
					// Remove the item from the vector
					enemybullets.erase(enemybullets.begin() + i);
				}
			}

			// Update the enemies
			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Update(frameTime);
				// If the bullet is dead, delete it
				if (!enemies[i].GetAlive())
				{
					// Remove the item from the vector
					enemies.erase(enemies.begin() + i);
				}
			}

			timeTospawn -= frameTime;

			// to see if this is time to spawn badguy

			if (timeTospawn <= sf::seconds(0) && spawnIndex < spawndata.size())
			{

				enemies.push_back(Ememy(EmemyTexture,
					gameWindow.getSize(),
					enemybullets,
					enemyBulletTexture,
					firingSoundBuffer,
					spawndata[spawnIndex].position,
					spawndata[spawnIndex].pattern));
				++spawnIndex;
				if (spawnIndex < spawndata.size())
					timeTospawn = spawndata[spawnIndex].delay;

			}

			// Checking if player builts hit enmey 
			// Note that we have i and j 
			for (int i = 0; i < bullets.size(); ++i)
			{
				for (int j = 0; j < enemies.size(); ++j)
				{

					sf::FloatRect bulletBounds = bullets[i].GetHitBox();
					sf::FloatRect enemyBound = enemies[j].GetHitBox();


					if (bulletBounds.intersects(enemyBound))
					{
						// they do ! 
						// kill the builter and badguy

						bullets[i].SetAlive(false);
						enemies[j].SetAlive(false);
						Playerscore = Playerscore + 100;

					}
				}
			}



			// Checking if Enmey builts hit Player 
			// Note that we have i and j 
			for (int i = 0; i < enemybullets.size(); ++i)
			{



				sf::FloatRect bulletBounds = enemybullets[i].GetHitBox();
				sf::FloatRect playerBound = playerObject.GetHitBox();


				if (bulletBounds.intersects(playerBound))
				{
				// Game Over !
				gameOver = true;
				gameMusic.pause();

				}

			}

			for (int j = 0; j < enemies.size(); ++j)
			{

				
				sf::FloatRect enemyBound = enemies[j].GetHitBox();
				sf::FloatRect playerBound = playerObject.GetHitBox();


				if (playerBound.intersects(enemyBound))
				{
					// Game Over !
					gameOver = true;
					gameMusic.pause();

				}
			}


			if (spawnIndex >= spawndata.size() && enemies.empty())
			{
				Win = true; 
				gameMusic.stop();
				// Add Vitory music 
			}


		}


		// Draw Section
		// -----------------------------------------------

		if (gameOver == false && !Win)
		{

			// Clear the window to a single colour
			gameWindow.clear(sf::Color::Black);

			for (int i = 0; i < Planets.size(); ++i)
			{
				Planets[i].DrawTo(gameWindow);
			}


			for (int i = 0; i < stars2.size(); ++i)
			{
				stars2[i].DrawTo(gameWindow);
			}

			for (int i = 0; i < bullets.size(); ++i)
			{
				bullets[i].DrawTo(gameWindow);
			}

			for (int i = 0; i < enemybullets.size(); ++i)
			{
				enemybullets[i].DrawTo(gameWindow);
			}

			// Draw everything to the window
			playerObject.Draw(gameWindow);

			// Show badguyz 

			for (int i = enemies.size() - 1; i >= 0; --i)
			{
				enemies[i].Draw(gameWindow);
				// If the bullet is dead, delete it

			}


			// Draw the stars
			for (int i = 0; i < stars.size(); ++i)
			{
				stars[i].DrawTo(gameWindow);
			}


			// Drawing Score Board 
			gameWindow.draw(scoreText);
		}
		if (gameOver == true) {
		
			gameWindow.draw(gameOverText);

		}

		if (Win) 
		{
			gameWindow.draw(WinText);
		}

		
		// TODO: Draw graphics
		// Display the window contents on the screen
		gameWindow.display();
	}
	// End of Game Loop
	return 0;
}
// End of main() Function