
#include "Ememy.h"
#include "Bullet.h"


Ememy::Ememy(sf::Texture& EmemyTexture, sf::Vector2u newScreenBounds, std::vector<Bullet>& newBullets, sf::Texture&newBulletTexture, sf::SoundBuffer&firingSoundBuffer, sf::Vector2f startringposition, std::vector<sf::Vector2f > newmomentPattern)
	: sprite(EmemyTexture)
	, velocity(0.0f, 0.0f)
	, speed(700.0f)
	, screenSize(newScreenBounds)
	, bullets(&newBullets)
	, bulletTexture(&newBulletTexture)
	, bulletCooldownRemaining(sf::seconds(0.0f))
	, bulletCooldownMax(sf::seconds(1.0f))
	, bulletFireSound(firingSoundBuffer)
	, momentPattern(newmomentPattern)
	, currentInstruction (0)
	, alive(true)
{
	// Avoid duplicating code by calling reset here instead of positioning the Ememy manually
	//Reset();


	sprite.setPosition(startringposition);
	//sprite.setPosition(500,500);

}



void Ememy::Update(sf::Time frameTime)
{
	// this stop the index justing above the array (vector)
	if (currentInstruction >= momentPattern.size())
	{
		// the enmeny when patter completed will kill them selfs
		alive = false;
		return;
	}

	// Get Tagert From instruction 

	sf::Vector2f targetPoint = momentPattern[currentInstruction] ;

	sf::Vector2f distanceVector = targetPoint - sprite.getPosition();

	// Call direction Vector (by maths)

	float distanceMag = std::sqrt(distanceVector.x*distanceVector.x + distanceVector.y*distanceVector.y);
	
	sf ::Vector2f directionVector = distanceVector / distanceMag;

  	float distancetoreavel = speed * frameTime.asSeconds();

	sf::Vector2f NewPosition = sprite.getPosition() + distanceVector * frameTime.asSeconds();

   	 
 	   //if (NewPosition.y + sprite.getTexture()->getSize().y > screenSize.y-35)
	//	NewPosition.y = screenSize.y - sprite.getTexture()->getSize().y-35;


	// Update the cooldown remaining for firing bullets
	bulletCooldownRemaining -= frameTime;

	// Check to see if we have reached target loc 

	if (distanceMag <= distancetoreavel) 
	{
		NewPosition = targetPoint;
		++currentInstruction;
		
	}

	// Move the Ememy
	sprite.setPosition(NewPosition);

	// If  the cooldown is up shoot the player is pressing space, spawn a bullet
	if ( bulletCooldownRemaining <= sf::seconds(0.0f))
	{

		sf::Vector2f bulletPosition = sprite.getPosition();
		bulletPosition.y += sprite.getTexture()->getSize().y / 2 - bulletTexture->getSize().y / 2;
		bulletPosition.x += sprite.getTexture()->getSize().x / 2 - bulletTexture->getSize().x / 2;
		bullets->push_back(Bullet(*bulletTexture, screenSize, bulletPosition, sf::Vector2f(-1000, 0)));
		// reset bullet cooldown
		bulletCooldownRemaining = bulletCooldownMax;
		// Play firing sound
		bulletFireSound.play();
	}



}

void Ememy::Draw(sf::RenderWindow& gameWindow)
{
	gameWindow.draw(sprite);
}

bool Ememy::GetAlive()
{
	return alive;
}
sf::FloatRect Ememy::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Ememy::SetAlive(bool newAlive)
{
	alive = newAlive;
}
