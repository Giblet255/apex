#pragma once
#include <SFML/Graphics.hpp> // this is the SFML package 
#include <vector>
#include <time.h> // see above


struct Spawndata
{
	sf::Vector2f position;
	std::vector<sf::Vector2f>pattern;
	sf::Time delay;

};