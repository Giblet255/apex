
#pragma once
// Library needed for using sprites, textures, and fonts
#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp> // this load in the sound buffers
// Library for handling collections of objects
#include <vector>
#include "Bullet.h"
class Ememy
{
public: // access level (to be discussed later)
// Constructor
	Ememy(sf::Texture& EmemyTexture, 
		sf::Vector2u newScreenBounds, 
		std::vector<Bullet>& newBullets, 
		sf::Texture&newBulletTexture, 
		sf::SoundBuffer&firingSoundBuffer,
		sf::Vector2f startringposition,
		std ::vector<sf::Vector2f > newmomentPattern);
	// Functions to call Ememy-specific code
	
	void Update(sf::Time frameTime);
	// Variables (data members) used by this class
	void Draw(sf::RenderWindow& gameWindow);

	// Getter
	bool GetAlive();
	sf::FloatRect GetHitBox();

	// Setter 
	void SetAlive(bool newAlive);

private:

	sf::Sprite sprite;
	sf::Vector2f velocity;
	float speed;
	sf::Vector2u screenSize;
	std::vector<Bullet>* bullets;
	sf::Texture* bulletTexture;
	sf::Time bulletCooldownRemaining;
	sf::Time bulletCooldownMax;
	sf::Sound bulletFireSound;
	std::vector<sf::Vector2f> momentPattern;
	int currentInstruction;
	bool alive;
	

};

