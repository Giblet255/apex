#include "Bullet.h"

Bullet::Bullet(sf::Texture& bulletTexture, sf::Vector2u newScreenBounds, sf::Vector2f startingPosition, sf::Vector2f newVelocity)
{
	sprite.setTexture(bulletTexture);
	screenBounds = newScreenBounds;
	sprite.setPosition(startingPosition);
	velocity = newVelocity;
	alive = true;
}
bool Bullet::GetAlive()
{
	return alive;
}


void Bullet::Update(sf::Time frameTime)
{
	// Calculate new position
	sf::Vector2f newPosition = sprite.getPosition() + velocity * frameTime.asSeconds();
	// Have we gone off the screen completely to the left or right?
	if (newPosition.x + sprite.getTexture()->getSize().x < 0
		|| newPosition.x > screenBounds.x)
	{
		// TODO: Our bullet is off the screen - we should delete it
	}
	// Move to the new position
	sprite.setPosition(newPosition);
}

void Bullet::DrawTo(sf::RenderTarget& target)
{
	target.draw(sprite);
} 


sf::FloatRect Bullet::GetHitBox()
{
	return sprite.getGlobalBounds();
}

void Bullet::SetAlive(bool newAlive)
{
	alive = newAlive;
}

